import androidx.compose.animation.EnterTransition
import androidx.compose.animation.ExitTransition
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import common.extensions.itemVariants
import model.viewModel.MainViewModel
import model.viewModel.SplitViewModel
import moe.tlaster.precompose.PreComposeApp
import moe.tlaster.precompose.koin.koinViewModel
import moe.tlaster.precompose.navigation.BackStackEntry
import moe.tlaster.precompose.navigation.NavHost
import moe.tlaster.precompose.navigation.PopUpTo
import moe.tlaster.precompose.navigation.rememberNavigator
import moe.tlaster.precompose.navigation.transition.NavTransition
import navigation.BottomMainNavigation
import navigation.Destination
import navigation.SplitOrderTopBar
import org.jetbrains.compose.ui.tooling.preview.Preview
import org.koin.compose.KoinContext
import scene.editOrder.EditOrderScene
import scene.payment.PaymentScene
import scene.split.SplitOrderScene
import scene.verifyOrder.VerifyOrderScene
import theme.StyledTheme

@Composable
@Preview
fun App() = PreComposeApp {
    KoinContext {
        StyledTheme {
            val navigator = rememberNavigator()
            val currentDestination by navigator.currentEntry.collectAsState(null)
            val mainViewModel = koinViewModel(MainViewModel::class)
            val splitViewModel = koinViewModel(SplitViewModel::class)

            mainViewModel.setupCategories()

            Scaffold(bottomBar = {
                if (!currentDestination.isInSplitMode) {
                    BottomMainNavigation(navigator = navigator, initialDestination = Destination.EDIT_ORDER)
                }
            }, topBar = {
                if (currentDestination.isInSplitMode) {
                    SplitOrderTopBar(navigator = navigator)
                }
            }, content = { padding ->
                NavHost(
                    modifier = Modifier.padding(padding),
                    navigator = navigator,
                    navTransition = navTransition,
                    initialRoute = Destination.MAIN.value,
                ) {
                    group(route = Destination.MAIN.value, initialRoute = Destination.EDIT_ORDER.value) {
                        scene(Destination.EDIT_ORDER.value) {
                            EditOrderScene()
                        }
                        scene(Destination.VERIFY_ORDER.value) {
                            VerifyOrderScene(onSplitOrder = {
                                splitViewModel.setupCategories(mainViewModel.categories.value)
                                navigator.navigate(Destination.SPLIT.value)
                            })
                        }
                        scene(Destination.PAYMENT.value) {
                            PaymentScene(totalPrice = mainViewModel.totalPrice, onCheckout = {
                                mainViewModel.resetOrder()
                                navigator.navigate(Destination.EDIT_ORDER.value)
                            })
                        }
                    }

                    group(route = Destination.SPLIT.value, initialRoute = Destination.SPLIT_ORDER.value) {
                        scene(Destination.SPLIT_ORDER.value) {
                            SplitOrderScene(
                                onPayClicked = { navigator.navigate(Destination.SPLIT_PAYMENT.value) },
                            )
                        }
                        scene(Destination.SPLIT_PAYMENT.value) {
                            PaymentScene(totalPrice = splitViewModel.totalPrice, onCheckout = {
                                val updatedVariants = splitViewModel.categories.value.itemVariants().map { variant ->
                                    variant.id to (variant.originalQuantity - variant.quantity)
                                }
                                mainViewModel.onQuantityChanged(updatedVariants)
                                navigator.goBack(PopUpTo(Destination.VERIFY_ORDER.value))
                            })
                        }
                    }
                }
            })
        }
    }
}

private val BackStackEntry?.isInSplitMode get() = this?.path == Destination.SPLIT.value || this?.path == Destination.SPLIT_PAYMENT.value

private val navTransition = NavTransition(
    createTransition = EnterTransition.None,
    resumeTransition = EnterTransition.None,
    destroyTransition = ExitTransition.None,
    pauseTransition = ExitTransition.None,
    enterTargetContentZIndex = 0f,
    exitTargetContentZIndex = 0f,
)