package di

import dev.gitlive.firebase.Firebase
import dev.gitlive.firebase.firestore.firestore
import model.viewModel.MainViewModel
import model.viewModel.SplitViewModel
import org.koin.dsl.module

fun mainModule() = module {
    single { MainViewModel(fireStore = get()) }
    single { SplitViewModel() }
    single { Firebase.firestore }
}