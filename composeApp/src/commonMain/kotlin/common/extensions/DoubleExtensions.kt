package common.extensions

fun Double.format(decimalPlaces: Int): String {
    val text = toString()
    val delimiterIndex = text.indexOf('.').takeIf { it >= 0 }

    return when (delimiterIndex == null) {
        true -> text
        false -> {
            val paddedText = text + "0".repeat(decimalPlaces)
            paddedText.removeRange(
                startIndex = (delimiterIndex + decimalPlaces + 1).coerceAtMost(paddedText.lastIndex),
                endIndex = paddedText.lastIndex + 1
            )
        }
    }
}

val Double.asEuroString get() = "${format(2)}€"
