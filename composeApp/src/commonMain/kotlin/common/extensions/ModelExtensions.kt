package common.extensions

import model.remote.Category
import model.remote.Item
import model.remote.ItemVariant
import kotlin.jvm.JvmName

@JvmName("mapVariantsForCategory")
fun List<Category>.mapVariants(block: (variant: ItemVariant) -> ItemVariant) = map { category ->
    category.copy(items = category.items.mapVariants(block))
}

@JvmName("mapVariantsForItems")
fun List<Item>.mapVariants(block: (variant: ItemVariant) -> ItemVariant) = map { item ->
    item.copy(itemVariants = item.itemVariants.map(block))
}

fun Category.getNonEmptyItems() = items.mapNotNull { item ->
    val chosenVariants = item.itemVariants.mapNotNull { variant -> variant.takeIf { it.quantity > 0 } }

    when (chosenVariants.isEmpty()) {
        true -> null
        false -> item.copy(itemVariants = chosenVariants)
    }
}

fun List<Category>.itemVariants() = flatMap { category -> category.items }.flatMap { item -> item.itemVariants }

fun ItemVariant.calculateNewPrice(newQuantity: Int) = (newQuantity - quantity) * price