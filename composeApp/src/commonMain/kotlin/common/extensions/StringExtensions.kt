package common.extensions

import androidx.compose.ui.graphics.Color

fun String.toQuantity() = sanitizeNumber(keepDelimiter = false).toIntOrNull()

fun String.toPrice() =
    sanitizeNumber(keepDelimiter = true).toDoubleOrNull()

private fun String.sanitizeNumber(keepDelimiter: Boolean) = removeNonDigits(keepDelimiter = keepDelimiter).handleZeros()

private fun String.removeNonDigits(keepDelimiter: Boolean) = when (keepDelimiter) {
    true -> replace(Regex("[^\\d,.]"), "")
        .replaceReoccurrence(oldValue = ",", newValue = "")
        .replaceReoccurrence(oldValue = ".", newValue = "")
        .removeSuffix(",")
        .removeSuffix(".")

    false -> replace(Regex("\\D"), "")
}

private fun String.handleZeros() = when {
    length > 1 -> removePrefix("0")
    isBlank() -> "0"
    else -> this
}

private fun String.replaceReoccurrence(oldValue: String, newValue: String): String {
    val stringParts = split(oldValue, limit = 2)

    if (stringParts.size == 1) return this

    return stringParts[0] + oldValue + stringParts[1].replace(oldValue, newValue)
}

val String.asColor get() = Color(removePrefix("#").toLong(16) or 0x00000000FF000000)
