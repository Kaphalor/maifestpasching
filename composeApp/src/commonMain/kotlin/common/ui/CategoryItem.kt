package common.ui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import model.remote.Category
import model.remote.Item
import model.remote.ItemVariant
import scene.editOrder.EditItemVariant

@Composable
fun CategoryItem(
    modifier: Modifier = Modifier,
    category: Category,
    chosenItems: List<Item>,
    onQuantityChanged: (itemVariant: ItemVariant, quantity: Int) -> Unit,
    description: @Composable ColumnScope.(ItemVariant) -> Unit,
) = Column(modifier = modifier) {
    Row(modifier = Modifier.padding(vertical = 4.dp), verticalAlignment = Alignment.CenterVertically) {
        Text(text = category.name, style = MaterialTheme.typography.h4)
    }
    chosenItems.map { item ->
        VariantItem(
            item = item,
            showFullItemName = category.items.first { it.id == item.id }.itemVariants.size > 1,
            onQuantityChanged = onQuantityChanged,
            description = description
        )
    }
}

@Composable
private fun VariantItem(
    item: Item,
    showFullItemName: Boolean,
    onQuantityChanged: (itemVariant: ItemVariant, quantity: Int) -> Unit,
    description: @Composable ColumnScope.(ItemVariant) -> Unit,
) = item.itemVariants.map { variant ->
    EditItemVariant(
        modifier = Modifier.padding(6.dp),
        variant = variant,
        title = when (showFullItemName) {
            true -> "${item.name} ${variant.name}"
            false -> item.name
        },
        onQuantityChanged = onQuantityChanged,
        description = description
    )
}