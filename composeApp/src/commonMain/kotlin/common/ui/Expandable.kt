package common.ui

import androidx.compose.animation.*
import androidx.compose.animation.core.keyframes
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.KeyboardArrowDown
import androidx.compose.material.icons.rounded.KeyboardArrowUp
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp

@Composable
fun Collapsable(
    modifier: Modifier = Modifier,
    isInitiallyExpanded: Boolean = false,
    header: @Composable () -> Unit,
    content: @Composable AnimatedVisibilityScope.() -> Unit
) = Column(modifier = modifier) {
    var isExpanded by remember { mutableStateOf(isInitiallyExpanded) }
    fun invertExpansionState() {
        isExpanded = !isExpanded
    }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable(enabled = true, onClick = ::invertExpansionState)
            .background(color = MaterialTheme.colors.surface)
            .padding(vertical = 6.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        header()
        IconButton(
            onClick = ::invertExpansionState,
            content = {
                ExpansionIcon(isExpanded)
            },
        )
    }

    Divider(color = MaterialTheme.colors.background)

    val fadeAnimationSpec = keyframes<Float> { durationMillis = ANIMATION_DURATION }
    val expandAnimationSpec = keyframes<IntSize> { durationMillis = ANIMATION_DURATION }
    AnimatedVisibility(
        visible = isExpanded,
        content = content,
        enter = fadeIn(animationSpec = fadeAnimationSpec) + expandVertically(animationSpec = expandAnimationSpec),
        exit = fadeOut(animationSpec = fadeAnimationSpec) + shrinkVertically(animationSpec = expandAnimationSpec)
    )
}

@Composable
private fun ExpansionIcon(isExpanded: Boolean) = when (isExpanded) {
    true -> Icon(imageVector = Icons.Rounded.KeyboardArrowUp, contentDescription = "Einklappen")
    false -> Icon(imageVector = Icons.Rounded.KeyboardArrowDown, contentDescription = "Ausklappen")
}

private const val ANIMATION_DURATION = 150