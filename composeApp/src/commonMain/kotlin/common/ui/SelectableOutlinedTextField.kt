package common.ui

import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.OutlinedTextField
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.TextFieldValue

@Composable
fun SelectableOutlinedTextField(
    modifier: Modifier = Modifier,
    text: String,
    label: @Composable () -> Unit,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    onValueChange: (String) -> Unit
) {
    var textSelection by remember { mutableStateOf(TextRange(0)) }
    var isSelected by remember { mutableStateOf(false) }
    val textFieldValue by derivedStateOf { TextFieldValue(text = text, selection = textSelection) }

    OutlinedTextField(
        modifier = modifier.onFocusChanged {
            if (!it.hasFocus) {
                isSelected = false
                return@onFocusChanged
            }

            if (!isSelected) {
                textSelection = TextRange(0, textFieldValue.text.length)
            }
        },
        value = textFieldValue,
        onValueChange = { value ->
            if (isSelected) {
                onValueChange(value.text)
                textSelection = value.selection
            } else {
                isSelected = true
            }
        },
        label = label,
        keyboardOptions = keyboardOptions,
    )
}