package common.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.luminance
import androidx.compose.ui.unit.dp

@Composable
fun TextChip(
    text: String,
    color: Color? = null,
) = Text(
    modifier = Modifier.background(
        color = color ?: Color.Transparent,
        shape = RoundedCornerShape(100)
    ).padding(vertical = 4.dp, horizontal = 8.dp),
    text = text,
    color = color.contrastColor,
)

private val Color?.contrastColor: Color
    get() {
        if (this == null) return Color.Unspecified

        return when (luminance() > 0.5) {
            true -> Color.Black
            false -> Color.White
        }
    }