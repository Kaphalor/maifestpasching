package common.ui

import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SelectableWrappedOutlinedTextField(
    text: String,
    onValueChange: (String) -> Unit,
    modifier: Modifier = Modifier,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    singleLine: Boolean = true,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    label: @Composable (() -> Unit)? = null,
    leadingIcon: @Composable (() -> Unit)? = null,
    trailingIcon: @Composable (() -> Unit)? = null,
    horizontalPadding: Dp = 8.dp,
    textFieldColors: TextFieldColors = TextFieldDefaults.outlinedTextFieldColors()
) {
    var textSelection by remember { mutableStateOf(TextRange(0)) }
    var isSelected by remember { mutableStateOf(false) }
    val textFieldValue by derivedStateOf { TextFieldValue(text = text, selection = textSelection) }

    BasicTextField(
        value = textFieldValue,
        onValueChange = { value ->
            if (isSelected) {
                onValueChange(value.text)
                textSelection = value.selection
            } else {
                isSelected = true
            }
        },
        interactionSource = interactionSource,
        singleLine = singleLine,
        modifier = modifier.width(IntrinsicSize.Min).onFocusChanged {
            if (!it.hasFocus) {
                isSelected = false
                return@onFocusChanged
            }

            if (!isSelected) {
                textSelection = TextRange(0, textFieldValue.text.length)
            }
        },
        keyboardOptions = keyboardOptions,
        cursorBrush = SolidColor(textFieldColors.cursorColor(false).value),
        textStyle = LocalTextStyle.current.copy(color = MaterialTheme.colors.onSurface)
    ) {
        TextFieldDefaults.OutlinedTextFieldDecorationBox(
            value = text,
            visualTransformation = VisualTransformation.None,
            innerTextField = it,
            singleLine = true,
            enabled = true,
            interactionSource = interactionSource,
            contentPadding = TextFieldDefaults.textFieldWithoutLabelPadding(
                start = horizontalPadding,
                end = horizontalPadding
            ),
            colors = TextFieldDefaults.outlinedTextFieldColors(),
            label = label,
            leadingIcon = leadingIcon,
            trailingIcon = trailingIcon,
        )
    }
}