package common.ui

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun FullWidthButton(
    text: String,
    onClick: () -> Unit,
) = Button(
    modifier = Modifier.fillMaxWidth().padding(horizontal = 12.dp, vertical = 12.dp),
    onClick = onClick,
    content = {
        Text(modifier = Modifier.padding(vertical = 6.dp), text = text)
    }
)