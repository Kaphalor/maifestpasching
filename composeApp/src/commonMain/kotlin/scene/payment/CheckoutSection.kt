package scene.payment

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import common.extensions.asEuroString

@Composable
fun CheckoutSection(
    modifier: Modifier = Modifier,
    returnAmount: Double,
    onCheckout: () -> Unit,
) = Row(
    modifier = modifier,
    horizontalArrangement = Arrangement.SpaceBetween,
    verticalAlignment = Alignment.CenterVertically
) {
    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        if (returnAmount >= 0) {
            Text(text = "Rückgeld", textAlign = TextAlign.Center, style = MaterialTheme.typography.h5)
            Text(
                text = returnAmount.asEuroString,
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.h6
            )
        }
    }

    Button(
        modifier = Modifier.padding(vertical = 6.dp),
        onClick = onCheckout,
        content = { Text("Abschließen") },
    )
}