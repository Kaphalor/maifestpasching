package scene.payment

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import theme.BillColor

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun BillSection(
    modifier: Modifier = Modifier,
    onBillClicked: (Int) -> Unit
) = FlowRow(
    modifier = modifier,
    horizontalArrangement = Arrangement.Center,
) {
    bills.map { (value, color) ->
        Button(
            modifier = Modifier.padding(horizontal = 16.dp, vertical = 8.dp),
            onClick = { onBillClicked(value) },
            colors = ButtonDefaults.buttonColors(backgroundColor = color)
        ) {
            Text(
                modifier = Modifier.padding(6.dp),
                text = "$value€",
                color = Color.Black,
                style = MaterialTheme.typography.h6
            )
        }
    }
}

private val bills = listOf(
    1 to BillColor.One,
    2 to BillColor.Two,
    5 to BillColor.Five,
    10 to BillColor.Ten,
    20 to BillColor.Twenty,
    50 to BillColor.Fifty,
    100 to BillColor.Hundred,
)