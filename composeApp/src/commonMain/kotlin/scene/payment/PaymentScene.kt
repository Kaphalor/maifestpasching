package scene.payment

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import common.extensions.asEuroString
import common.extensions.format
import common.extensions.toPrice
import common.ui.SelectableOutlinedTextField

@Composable
fun PaymentScene(
    totalPrice: Double,
    onCheckout: () -> Unit,
) = Column(
    modifier = Modifier.fillMaxSize(),
    verticalArrangement = Arrangement.Center,
    horizontalAlignment = Alignment.CenterHorizontally,
) {
    var givenAmount by remember { mutableStateOf(0.0) }

    Text(text = totalPrice.asEuroString, style = MaterialTheme.typography.h5)

    SelectableOutlinedTextField(
        text = givenAmount.format(2),
        onValueChange = { text ->
            givenAmount = (text.toPrice() ?: givenAmount)
        },
        label = {
            Text("Gegeben", textAlign = TextAlign.Center)
        },
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number, imeAction = ImeAction.Done),
    )

    BillSection(
        modifier = Modifier.fillMaxWidth().padding(top = 32.dp),
        onBillClicked = { billValue -> givenAmount += billValue }
    )

    CheckoutSection(
        modifier = Modifier.fillMaxWidth().padding(horizontal = 64.dp, vertical = 32.dp),
        returnAmount = givenAmount - totalPrice,
        onCheckout = onCheckout
    )
}