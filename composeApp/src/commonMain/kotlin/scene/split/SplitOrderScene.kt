package scene.split

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import common.ui.CategoryItem
import common.ui.FullWidthButton
import model.viewModel.SplitViewModel
import moe.tlaster.precompose.koin.koinViewModel
import theme.paddingTop

@Composable
fun SplitOrderScene(
    onPayClicked: () -> Unit,
    splitViewModel: SplitViewModel = koinViewModel(SplitViewModel::class),
) = Column {
    val splitCategories by splitViewModel.categories.collectAsState()

    LazyColumn(modifier = Modifier.padding(start = 16.dp, end = 16.dp, top = 8.dp).fillMaxHeight().weight(0.1f)) {
        itemsIndexed(splitCategories) { index, category ->

            if (category.items.isNotEmpty()) {
                CategoryItem(
                    modifier = Modifier.padding(top = paddingTop(index)),
                    category = category,
                    chosenItems = category.items,
                    onQuantityChanged = splitViewModel::onQuantityChanged,
                    description = { variant ->
                        Text("Bestellt: ${variant.originalQuantity}")
                    }
                )
            }
        }
    }

    FullWidthButton(
        text = "Bezahlen",
        onClick = { onPayClicked() },
    )
}