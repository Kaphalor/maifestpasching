package scene.verifyOrder

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import common.extensions.asColor
import common.extensions.asEuroString
import common.extensions.getNonEmptyItems
import common.ui.CategoryItem
import common.ui.FullWidthButton
import common.ui.TextChip
import model.viewModel.MainViewModel
import moe.tlaster.precompose.koin.koinViewModel
import theme.paddingTop

@Composable
fun VerifyOrderScene(
    mainViewModel: MainViewModel = koinViewModel(MainViewModel::class),
    onSplitOrder: () -> Unit,
) = Column(
    modifier = Modifier.fillMaxSize(),
    horizontalAlignment = Alignment.CenterHorizontally,
    verticalArrangement = Arrangement.Center,
) {
    val categories by mainViewModel.categories.collectAsState()

    if (mainViewModel.totalPrice == 0.0) {
        Text(text = "Keine offene Bestellung", style = MaterialTheme.typography.h3, textAlign = TextAlign.Center)
        return
    }

    LazyColumn(modifier = Modifier.padding(start = 16.dp, end = 16.dp, top = 16.dp).fillMaxHeight().weight(0.1f)) {
        itemsIndexed(categories) { index, category ->
            val chosenItems = category.getNonEmptyItems()

            if (chosenItems.isNotEmpty()) {
                CategoryItem(
                    modifier = Modifier.padding(top = paddingTop(index)),
                    category = category,
                    chosenItems = chosenItems,
                    onQuantityChanged = { variant, quantity ->
                        mainViewModel.onQuantityChanged(
                            quantityId = variant.id,
                            quantity = quantity
                        )
                    },
                    description = { variant ->
                        TextChip(
                            text = variant.price.asEuroString,
                            color = variant.colorCode?.asColor
                        )
                    }
                )
            }
        }
    }

    FullWidthButton(
        text = "Bestellung Trennen",
        onClick = onSplitOrder,
    )
}