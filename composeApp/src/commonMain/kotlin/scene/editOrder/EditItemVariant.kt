package scene.editOrder

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Add
import androidx.compose.material.icons.rounded.Remove
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import common.extensions.toQuantity
import common.ui.SelectableWrappedOutlinedTextField
import model.remote.ItemVariant

@Composable
fun EditItemVariant(
    modifier: Modifier = Modifier,
    variant: ItemVariant,
    title: String = variant.name,
    onQuantityChanged: (itemVariant: ItemVariant, quantity: Int) -> Unit,
    description: @Composable ColumnScope.(ItemVariant) -> Unit,
) = Row(
    modifier = modifier.fillMaxWidth(),
    horizontalArrangement = Arrangement.SpaceBetween,
    verticalAlignment = Alignment.CenterVertically,
) {
    Column(modifier = Modifier.weight(0.1f)) {
        Text(
            text = title,
            style = MaterialTheme.typography.h5,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
        )
        description(variant)
    }

    SelectableWrappedOutlinedTextField(
        text = variant.quantity.toString(),
        onValueChange = { text ->
            onQuantityChanged(variant, text.toQuantity() ?: variant.quantity)
        },
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number, imeAction = ImeAction.Done),
        horizontalPadding = 16.dp,
        leadingIcon = {
            IconButton(
                onClick = { onQuantityChanged(variant, variant.quantity - 1) },
            ) {
                Icon(imageVector = Icons.Rounded.Remove, "Menge von ${variant.name} reduzieren")
            }
        },
        trailingIcon = {
            IconButton(
                onClick = { onQuantityChanged(variant, variant.quantity + 1) },
            ) {
                Icon(imageVector = Icons.Rounded.Add, "Menge von ${variant.name} erhöhen")
            }
        },
    )
}