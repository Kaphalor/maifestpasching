package scene.editOrder

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import model.viewModel.MainViewModel
import moe.tlaster.precompose.koin.koinViewModel

@Composable
fun EditOrderScene(mainViewModel: MainViewModel = koinViewModel(MainViewModel::class)) {
    val currentCategoryId by mainViewModel.currentCategoryId.collectAsState()
    val categories by mainViewModel.categories.collectAsState()

    if (currentCategoryId == null) {
        EditOrderHome(
            categories = categories,
            setCurrentCategory = mainViewModel::setCurrentCategory
        )
    } else {
        EditCategory(
            category = categories.find { it.id == currentCategoryId },
            onQuantityChanged = { variant, quantity ->
                mainViewModel.onQuantityChanged(
                    quantityId = variant.id,
                    quantity = quantity
                )
            },
            onClose = mainViewModel::resetCurrentCategory,
        )
    }
}
