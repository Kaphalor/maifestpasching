package scene.editOrder

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Close
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import common.extensions.asColor
import common.extensions.asEuroString
import common.ui.Collapsable
import common.ui.TextChip
import model.remote.Category
import model.remote.Item
import model.remote.ItemVariant
import theme.paddingTop

@Composable
fun EditCategory(
    category: Category?,
    onClose: () -> Unit,
    onQuantityChanged: (itemVariant: ItemVariant, quantity: Int) -> Unit,
) {
    if (category == null) return

    Column {
        Row(
            modifier = Modifier.fillMaxWidth().padding(16.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(text = category.name, style = MaterialTheme.typography.h4)
            IconButton(
                modifier = Modifier.background(color = MaterialTheme.colors.surface, shape = CircleShape),
                onClick = { onClose() },
                content = {
                    Icon(imageVector = Icons.Rounded.Close, contentDescription = "Schließen")
                },
            )
        }

        Column {
            category.items.map { item ->
                CategoryItem(item = item, onQuantityChanged = onQuantityChanged)
            }
        }
    }
}

@Composable
private fun CategoryItem(
    item: Item,
    onQuantityChanged: (itemVariant: ItemVariant, quantity: Int) -> Unit,
) = Collapsable(header = {
    Text(modifier = Modifier.padding(horizontal = 6.dp), text = item.name, style = MaterialTheme.typography.h4)
}, content = {
    LazyColumn(modifier = Modifier.padding(horizontal = 12.dp, vertical = 6.dp)) {
        itemsIndexed(item.itemVariants) { index, variant ->
            EditItemVariant(
                modifier = Modifier.padding(top = paddingTop(index)),
                variant = variant,
                onQuantityChanged = onQuantityChanged,
                description = {
                    TextChip(
                        text = variant.price.asEuroString,
                        color = variant.colorCode?.asColor
                    )
                }
            )
        }
    }
})
