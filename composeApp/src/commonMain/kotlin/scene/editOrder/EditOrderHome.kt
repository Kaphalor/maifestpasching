package scene.editOrder

import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import model.remote.Category

@Composable
fun EditOrderHome(categories: List<Category>, setCurrentCategory: (String) -> Unit) = Column(
    modifier = Modifier.fillMaxSize(),
    horizontalAlignment = Alignment.CenterHorizontally,
    verticalArrangement = Arrangement.Center,
) {
    fun onClick(category: Category): () -> Unit = {
        setCurrentCategory(category.id)
    }

    categories.chunked(2) { it[0] to it.getOrNull(1) }.forEach { (leftItem, rightItem) ->
        Row(
            modifier = Modifier.fillMaxWidth(0.9f).padding(top = 16.dp, bottom = 16.dp),
            horizontalArrangement = Arrangement.SpaceEvenly
        ) {
            OrderItem(
                text = leftItem.name, onClick = onClick(leftItem)
            )
            Spacer(Modifier.weight(0.1f))
            if (rightItem == null) {
                Spacer(Modifier.weight(1f))
            } else {
                OrderItem(
                    text = rightItem.name, onClick = onClick(rightItem)
                )
            }
        }
    }
}

@Composable
private fun RowScope.OrderItem(text: String, onClick: () -> Unit) = Button(
    modifier = Modifier.weight(1f).aspectRatio(1f), onClick = onClick, shape = MaterialTheme.shapes.large
) {
    Text(text.replace(" ", "\n"), style = MaterialTheme.typography.h4, textAlign = TextAlign.Center)
}