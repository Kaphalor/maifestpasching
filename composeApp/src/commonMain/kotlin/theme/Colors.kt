package theme

import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.ui.graphics.Color

val LightColors = lightColors(
    primary = Color(0xffce4024),
    surface = Color(0xffc1c1c1),
)

val DarkColors = darkColors(
    primary = Color(0xffff5d2c),
    surface = Color(0xff2e2e2e),
)

object BillColor {
    val One = Color(0xffaf8e55)
    val Two = Color(0xffb4b6b6)
    val Five = Color(0xffd9d4a6)
    val Ten = Color(0xffda817e)
    val Twenty = Color(0xff72abd0)
    val Fifty = Color(0xffE5A369)
    val Hundred = Color(0xffB5D493)
}
