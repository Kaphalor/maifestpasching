package theme

import androidx.compose.ui.unit.dp

fun paddingTop(index: Int) = if (index == 0) 0.dp else 6.dp