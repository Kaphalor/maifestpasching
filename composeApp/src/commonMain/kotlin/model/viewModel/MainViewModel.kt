package model.viewModel

import common.extensions.calculateNewPrice
import common.extensions.mapVariants
import dev.gitlive.firebase.firestore.DocumentSnapshot
import dev.gitlive.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import model.remote.Category
import model.remote.Item
import model.remote.ItemVariant
import moe.tlaster.precompose.viewmodel.ViewModel
import moe.tlaster.precompose.viewmodel.viewModelScope

class MainViewModel(private val fireStore: FirebaseFirestore) : ViewModel() {
    private val _currentCategoryId = MutableStateFlow<String?>(null)
    val currentCategoryId: StateFlow<String?> = _currentCategoryId

    private val _categories = MutableStateFlow(emptyList<Category>())
    val categories: StateFlow<List<Category>> = _categories

    var totalPrice: Double = 0.0
        private set

    fun setupCategories() = viewModelScope.launch {
        val categoryPath = "Categories"
        val remoteCategories = fireStoreCollection(categoryPath).map { categoryDocument ->
            val itemPath = "$categoryPath/${categoryDocument.id}/Items"

            val remoteItems = fireStoreCollection(itemPath).map { itemDocument ->
                val variantPath = "$itemPath/${itemDocument.id}/ItemVariants"
                val remoteVariants = fireStoreCollection(variantPath).itemVariants().sortedBy(ItemVariant::position)

                itemDocument.items(remoteVariants)
            }.sortedBy(Item::position)

            categoryDocument.categories(remoteItems)
        }.sortedBy(Category::position)

        _categories.update { remoteCategories }
    }

    private suspend fun fireStoreCollection(collectionPath: String): List<DocumentSnapshot> =
        fireStore.collection(collectionPath).get().documents

    private fun DocumentSnapshot.categories(items: List<Item>) = data<Category>().copy(items = items)

    private fun DocumentSnapshot.items(variants: List<ItemVariant>) = data<Item>().copy(itemVariants = variants)

    private fun List<DocumentSnapshot>.itemVariants() = map { document -> document.data<ItemVariant>() }

    fun setCurrentCategory(categoryId: String) = _currentCategoryId.update { categoryId }

    fun resetCurrentCategory() = _currentCategoryId.update { null }

    fun onQuantityChanged(quantityId: String, quantity: Int) = onQuantityChanged(
        listOf(quantityId to quantity)
    )

    fun onQuantityChanged(newQuantities: List<Pair<String, Int>>) {
        val sanitizedQuantities = newQuantities.filter { (_, quantity) -> quantity >= 0 }
        if (sanitizedQuantities.size < 0) return

        _categories.update { categories ->
            categories.mapVariants { variant ->
                val newQuantity = sanitizedQuantities.firstOrNull { (variantId, _) -> variantId == variant.id }

                when (newQuantity == null) {
                    true -> variant
                    false -> {
                        val (_, quantity) = newQuantity
                        totalPrice += variant.calculateNewPrice(newQuantity = quantity)
                        variant.copy(quantity = quantity)
                    }
                }
            }
        }
    }

    fun resetOrder() {
        _categories.update { categories ->
            categories.mapVariants { variant ->
                variant.copy(quantity = 0)
            }
        }
        totalPrice = 0.0
    }
}