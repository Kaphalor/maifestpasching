package model.viewModel

import common.extensions.calculateNewPrice
import common.extensions.getNonEmptyItems
import common.extensions.mapVariants
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import model.remote.Category
import model.remote.ItemVariant
import moe.tlaster.precompose.viewmodel.ViewModel

class SplitViewModel : ViewModel() {
    private val _categories = MutableStateFlow(emptyList<Category>())
    val categories: StateFlow<List<Category>> = _categories

    var totalPrice: Double = 0.0
        private set

    fun setupCategories(originalCategories: List<Category>) {
        totalPrice = 0.0
        _categories.update {
            originalCategories.map { category ->
                category.copy(
                    items = category.getNonEmptyItems().mapVariants { variant ->
                        variant.copy(quantity = 0, originalQuantity = variant.quantity)
                    }
                )
            }
        }
    }

    fun onQuantityChanged(itemVariant: ItemVariant, quantity: Int) {
        if (quantity !in 0..itemVariant.originalQuantity) return

        _categories.update { categories ->
            categories.mapVariants { variant ->
                when (variant.id == itemVariant.id) {
                    true -> {
                        totalPrice += itemVariant.calculateNewPrice(newQuantity = quantity)
                        variant.copy(quantity = quantity)
                    }

                    false -> variant
                }
            }
        }
    }
}