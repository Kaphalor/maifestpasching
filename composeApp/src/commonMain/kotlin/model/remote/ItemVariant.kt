package model.remote

import kotlinx.serialization.Serializable

@Serializable
data class ItemVariant(
    val id: String,
    val position: Int,
    val name: String,
    val price: Double,
    val quantity: Int = 0,
    val originalQuantity: Int = 0,
    val colorCode: String? = null,
)