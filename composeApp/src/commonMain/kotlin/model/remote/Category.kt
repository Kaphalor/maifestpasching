package model.remote

import kotlinx.serialization.Serializable

@Serializable
data class Category(
    val id: String,
    val position: Int,
    val name: String,
    val items: List<Item> = emptyList()
)