package model.remote

import kotlinx.serialization.Serializable

@Serializable
data class Item(
    val id: String,
    val position: Int,
    val name: String,
    val itemVariants: List<ItemVariant> = emptyList()
)
