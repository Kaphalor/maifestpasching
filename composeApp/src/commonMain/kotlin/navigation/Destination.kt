package navigation

import kotlin.jvm.JvmInline

@JvmInline
value class Destination private constructor(val value: String) {
    companion object {
        val MAIN = Destination("/MAIN")
        val EDIT_ORDER = Destination("/EDIT_ORDER")
        val VERIFY_ORDER = Destination("/VERIFY_ORDER")
        val PAYMENT = Destination("/PAYMENT")

        val SPLIT = Destination("/SPLIT")
        val SPLIT_ORDER = Destination("/SPLIT_ORDER")
        val SPLIT_PAYMENT = Destination("/SPLIT_PAYMENT")
    }
}