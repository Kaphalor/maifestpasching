package navigation

import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import moe.tlaster.precompose.navigation.BackStackEntry
import moe.tlaster.precompose.navigation.Navigator

@Composable
fun BottomMainNavigation(navigator: Navigator, initialDestination: Destination) {
    val backStackEntry by navigator.currentEntry.collectAsState(null)
    fun isSelected(destination: Destination) =
        destination.value == backStackEntry.currentDestination(defaultDestination = initialDestination)

    BottomNavigation {
        BottomNavigationItem(
            selected = isSelected(Destination.EDIT_ORDER),
            onClick = { navigator.navigate(Destination.EDIT_ORDER.value) },
            icon = {},
            label = { Text("Bestellen", style = MaterialTheme.typography.body2) }
        )
        BottomNavigationItem(
            selected = isSelected(Destination.VERIFY_ORDER),
            onClick = { navigator.navigate(Destination.VERIFY_ORDER.value) },
            icon = {},
            label = { Text("Prüfen", style = MaterialTheme.typography.body2) }
        )
        BottomNavigationItem(
            selected = isSelected(Destination.PAYMENT),
            onClick = { navigator.navigate(Destination.PAYMENT.value) },
            icon = {},
            label = { Text("Bezahlen", style = MaterialTheme.typography.body2) }
        )
    }
}

private fun BackStackEntry?.currentDestination(defaultDestination: Destination) =
    this?.route?.route?.takeUnless { it == Destination.MAIN.value } ?: defaultDestination.value
