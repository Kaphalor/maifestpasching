package navigation

import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.runtime.Composable
import moe.tlaster.precompose.navigation.Navigator

@Composable
fun SplitOrderTopBar(navigator: Navigator) = TopAppBar(
    title = { Text("Bestellung Trennen") },
    navigationIcon = { BackButton(navigator = navigator) }
)

@Composable
private fun BackButton(navigator: Navigator) = IconButton(
    content = {
        Icon(
            imageVector = Icons.AutoMirrored.Default.ArrowBack,
            contentDescription = "Zurück"
        )
    },
    onClick = navigator::goBack
)